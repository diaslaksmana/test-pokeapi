export interface PokeModal{
    id:number;
    name:string;
    sprites:string;
    front_default:string;
    url:string;
    flavorText:string;

}
export interface GetPokeResponse {
    results: PokeModal[];
  }
  