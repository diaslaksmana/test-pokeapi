
import { Route, Routes } from "react-router";
import Detail from "../page/Detail";
import Home from "../page/Home";

const Router = () => {
    return (
      <Routes >
              <Route  path="" element={<Home />}/>
              <Route  path="home" element={<Home />}/>
              <Route path="/detail" element={<Detail />}>
                <Route  path=":name"/>
            </Route>
      </Routes>
    )
  }
  export default Router;