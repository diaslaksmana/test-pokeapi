import React from 'react';
import logo from './logo.svg';
import './App.css';
import Router from './router/Index';
import '../src/style/main.scss'

function App() {
  return (
    <>
       <Router/>
    </>
  );
}

export default App;
