import  { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { PokeModal } from '../utils/interface/Poke';

export const Header = () => {
    const [listPoke, setListPoke] = useState<PokeModal[]>([]);
    const [query, setQuery] = useState("");
    const [suggestions, setSuggestions] = useState<PokeModal[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch("https://pokeapi.co/api/v2/pokemon?limit=100");
      const data = await response.json();
      setListPoke(data.results);
    };
    fetchData();
  }, []);

  const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    const query = event.target.value;
    setQuery(query);

    const filteredPokemon = listPoke.filter((pokemon) =>
      pokemon.name.startsWith(query)
    );

    setSuggestions(filteredPokemon);
  };

  return (
    <header id='header'>
        <div className='container'>
            <nav className='header'>
                <div className='header__left'>
                    <h1 className='logo'>Pokemon</h1>
                </div>
                <div className='header__right'>
                    <div className='search'>
                        <input placeholder='Find Pokemon .....' className='form-control' type="text" value={query} onChange={handleSearch} />
                        {(query !== "") && (
                            <ul className='sugesstion'>
                            {suggestions?.map((poke) => (
                                <li key={poke.name} >
                               <Link to={`/detail/${poke.name}`}>
                                    <p>{poke.name}</p>
                                </Link>
                                </li>
                            ))}
                            </ul>
                        )}
                    </div>
                    
      
                </div>
            </nav>
        </div>
    </header>
  )
}

export default Header
