import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import Header from '../layout/Header';
import { PokeModal } from '../utils/interface/Poke';

export const Home = () => {
    const [data, setData] = useState<PokeModal[]>([]);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState<any>();
    const [currentPage, setCurrentPage] = useState(1);
    useEffect(() => {
        try{
            const fetchData = async () => {
                const response = await fetch(`https://pokeapi.co/api/v2/pokemon/?limit=100`); 
                const data = await response.json();
                setData(data.results);      
                console.log(data)

              }
              
        fetchData();
        }catch (error) {
            setError(error);
        } finally {
            setIsLoading((false));
        }
    
      }, []);

      const loadMore = () => {
        setCurrentPage(currentPage + 1);
      };
      const itemsPerPage = 10;
      const startIndex = (currentPage - 1) * itemsPerPage;
      const endIndex = startIndex + itemsPerPage;

      const currentPokemonList = data.slice(startIndex, endIndex);

      if (isLoading) {
        return  <div>Loading....</div>;
      }
    
      if (error) {
        return <div>Error</div>;
      }
  return (
    <>
      <Header/>
      <section id='home'>
        <div className='container'>
          <ul className='listing'>
              {currentPokemonList?.map((poke) => (    
              <li key={poke.name}>
                <div className='card'>
                  <Link to={`/detail/${poke.name}`}>
                    <img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${poke.url.split('/')[6]}.png`} alt={poke.name}/>
                    <h4>{poke.name}</h4>
                  </Link>
                </div>                  
              </li>
              ))}               
          </ul>
          <div className='load__more'>
          {data.length > endIndex && (
                  <button className='btn' onClick={loadMore}>Load More</button>
                )}
          </div>
          
        </div>
      </section>
        
    </>
  )
}

export default Home