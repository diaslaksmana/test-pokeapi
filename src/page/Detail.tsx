import { useEffect, useState } from 'react'
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import { PokeModal } from '../utils/interface/Poke';

export const Detail = () => {
  const params = useParams();
  const [data, setData] = useState<PokeModal>();
  const [abilities, setAbilities] = useState<PokeModal>();
  const [species, setSpecies] = useState<PokeModal>();
  const [types, setTypes] = useState<PokeModal>();
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState<any>();
  useEffect(() => {
    try{
        const fetchData = async () => {
            const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${params.name}`); 
            const data = await response.json();
            setData(data);  
            setAbilities(data.abilities)     
            setSpecies(data.species)    
            setTypes(data.types)       
            console.log(data)
          }
          
    fetchData();
    }catch (error) {
        setError(error);
    } finally {
        setIsLoading((false));
    }

  }, []);

  if (isLoading) {
    return  <div>Loading....</div>;
  }

  if (error) {
    return <div>Error</div>;
  }
  return (
    <section id='detail'>
      <div className='container'>      
      
        <ul className='list__img'>
          <li>
            {/* @ts-ignore*/}
            <img src={data?.sprites?.back_default}/>
          </li>
          <li>
            {/* @ts-ignore*/}
            <img src={data?.sprites?.back_female}/>
          </li>
          <li>
            {/* @ts-ignore*/}
            <img src={data?.sprites?.back_shiny}/>
          </li>
          <li>
            {/* @ts-ignore*/}
            <img src={data?.sprites?.back_shiny_female}/>
          </li>
          <li>
            {/* @ts-ignore*/}
            <img src={data?.sprites?.front_default}/>
          </li>
          <li>
            {/* @ts-ignore*/}
            <img src={data?.sprites?.front_female}/>
          </li>
          <li>
            {/* @ts-ignore*/}
            <img src={data?.sprites?.front_shiny}/>
          </li>
        </ul>
        <h4>Name : {data?.name}</h4>
        <h4>Ability</h4>
            
        <ul>
          {/* @ts-ignore*/}  
          {abilities?.map((poke) => (    
            <li key={poke.url}>{poke?.ability.name} </li>
          ))} 
        </ul>

        <h4>Type</h4>
            
        <ul>
          {/* @ts-ignore*/}  
          {types?.map((poke) => (    
            <li key={poke.url}>{poke?.type?.name} </li>
          ))} 
        </ul>
       
        <h4>Species</h4>
        <p>{species?.name} </p>
        <p>{species?.flavorText}</p>

        <div>
          <Link to='/' className='btn'>Back to Home</Link>
        </div>
      </div>
     
    </section>
  )
}

export default Detail